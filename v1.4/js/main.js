/*!
 * Copyright NDOY3M4N
 * Released under the MIT license
 *
 */

$(document).ready(function() {
    // Partie qui va gérer les tabs
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tabcontent').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });

    // Partie qui va gérer l'affichage du navbar pour les appareils mobiles
    // PETIT PBME SUR CETTE PARTIE $(".hidden-menu, nav ul li")
    $(".hidden-menu, nav ul li").click(function() {
        $(".hidden-menu").toggleClass('open');
        $("nav").slideToggle(400, function() {
            $(this).toggleClass("nav-expand").css('display', '');
        });
    });

    // On affiche le bouton pour scroller en haut de la page
    function scroll_vers_haut() {
        $('#myBtn').click(function() {
            $('html,body').animate({scrollTop: 0}, 'slow');
        });
    // On change la couleur du navbar si on scroll 100px en partant du haut
    $(window).scroll(function(){
        if($(window).scrollTop() < 200){
            $('#myBtn').fadeOut();
            $('nav').removeClass('shrink');
        } else{
            $('#myBtn').fadeIn();
            $('nav').addClass('shrink');
        }
    });
    }
    scroll_vers_haut("#myBtn");

    //Durée en milliseconde pour le carousel
    // $('.carousel').carousel({ interval: 2500 });
});
